package com.hcl.restservice.atm.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import com.hcl.restservice.atm.VOs.MemberVo;
import com.hcl.restservice.atm.business.RetrieveMember;

@Path("/members")
public class ATMService 
{
	@GET
	@Path("/{memberId}")
	public Response getMemberInfo(@PathParam("memberId") String memberId)
	{
//		Validation
		if(memberId == null)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
//		Business logic process
		MemberVo member = RetrieveMember.getMemberById(memberId);
//		validate object
		if(member.getId().isEmpty())
		{
			return Response.status(Status.NOT_FOUND).build();
		}
//		return response
		return Response.status(Status.OK).entity(member).build();
	}
}