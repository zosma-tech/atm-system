package com.hcl.restservice.atm.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.hcl.restservice.atm.service.ATMService;

@ApplicationPath("/v1/bank/atm-management")
public class ApplicationService extends Application
{
	@Override
	public Set<Class<?>> getClasses() 
	{
		Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(ATMService.class);
		return super.getClasses();
	}
}
