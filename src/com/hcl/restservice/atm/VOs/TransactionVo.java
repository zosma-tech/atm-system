package com.hcl.restservice.atm.VOs;

import com.hcl.restservice.atm.enumTypes.TransactionTypes;

public class TransactionVo {
	
	private String id;
	private AccountVo account;
	private double amount;
	private TransactionTypes transaction_type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public AccountVo getAccount() {
		return account;
	}
	public void setAccount(AccountVo account) {
		this.account = account;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public TransactionTypes getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(TransactionTypes transaction_type) {
		this.transaction_type = transaction_type;
	}
	
	
}
