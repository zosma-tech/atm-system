package com.hcl.restservice.atm.VOs;

import com.hcl.restservice.atm.enumTypes.AccountTypes;

public class AccountVo {
	private String accountName;
	private AccountTypes accountType;
	private String creationDate;
	private String status;
	private double balance;
	
	private String accountNumber;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public AccountTypes getAccountType() {
		return accountType;
	}
	public void setAccountType(AccountTypes accountType) {
		this.accountType = accountType;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}	
}