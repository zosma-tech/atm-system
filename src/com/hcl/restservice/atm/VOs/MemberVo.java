package com.hcl.restservice.atm.VOs;

import java.util.List;

/**
 *
 * @author Carlos
 */
public class MemberVo extends PersonVo {
 
    private String id;
    private List<AccountVo> accountList;
    

    //getter & setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public List<AccountVo> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountVo> accountList) {
        this.accountList = accountList;
    }
}
