package com.hcl.restservice.atm.VOs;

/**
 *
 * @author Carlos
 */
public class PersonVo {
    private String  name;
    private String age;
    private String phone;


    
    //getters & setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }  
    
}
