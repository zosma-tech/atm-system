package com.hcl.restservice.atm.utils;

import java.sql.*;

public class DBconnection {
	
	public Connection openConnection() throws SQLException
	{
        Connection connection;
        try
        {
	        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/rest","root","1234");
            System.out.println("Successfull connected");
        }
        catch(SQLException e)
        {
            System.out.println("Error To Connect To DataBase");
            connection = null;
        }
        return connection;
    }
    
    public void closeConnection (Connection c) throws SQLException{
        try
        {
            if(!c.isClosed())
            {
                c.close();
            }
        }
        catch(SQLException e)
        {
            System.out.println("Error to Close Connection To DataBase");
        }
    }

}
